<!doctype html>
<html lang="tr">
<head>
    <?php $this->load->view("includes/head"); ?>
    <?php $this->load->view("includes/include_style"); ?>
</head>
<body>

<!-- Header -->
    <?php $this->load->view("includes/header"); ?>
<!-- #Header -->



<!-- text -->
    <?php $this->load->view("about/content"); ?>
<!-- #text -->



    <?php $this->load->view("includes/include_script"); ?>
    <?php $this->load->view("includes/footer"); ?>

</body>
</html>