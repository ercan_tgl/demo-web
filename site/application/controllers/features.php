
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Features extends CI_Controller {

	public function index()
	{

		$this->load->model('about_model');
		$this->load->model('pages_model');

        $viewData = new stdClass();

       
        $viewData->about = $this->about_model->get_all(array(),"id ASC");
        $viewData->contents = $this->pages_model->get_all(array("page_url" => "home"),"rank ASC");
         $viewData->rows = $this->pages_model->get_all(array("isActive" => "1"),"rank ASC");

         $viewData->title       = 'Features';

        // $viewData->home = $this->db->get('pages')->result();
        

		$this->load->view('features', $viewData);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
