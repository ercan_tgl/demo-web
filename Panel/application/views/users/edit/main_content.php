<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <!-- form start -->
                <form role="form" method="post" action="<?php echo base_url("users/edit/$row->id");?>">
                    <div class="box-body col-md-6">
                        <div class="form-group"> Adı</label>
                            <input type="text" class="form-control" name="name" placeholder="Kullanıcı adını giriniz.." value="<?php echo $row->name; ?>">                        </div>
                             <div class="form-group">
                            <label for="exampleInputEmail1"> SoyAdı</label>
                            <input type="text" class="form-control" name="surname" placeholder="Kullanıcı SoyAdı giriniz.." value="<?php echo $row->surname; ?>">                        </div>
                             <div class="form-group">
                            <label for="exampleInputEmail1">Email</label>
                            <input type="text" class="form-control" name="email" placeholder="Email giriniz.." value="<?php echo $row->email; ?>">                        </div>
                             <div class="form-group">
                            <label for="exampleInputEmail1">Şifre</label>
                            <input type="text" class="form-control" name="password" placeholder="Şifre giriniz.." value="<?php echo $row->password; ?>">                        </div>
                             <div class="form-group">
                            <label for="exampleInputEmail1">Kullanıcı Adı</label>
                            <input type="text" class="form-control" name="username" placeholder="Kullanıcı Adı giriniz.." value="<?php echo $row->username; ?>">                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="clearfix"></div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Kaydet</button>
                    </div>
                </form>
            </div>
            <!-- /.box -->
        </div>
</section>
<!-- /.content -->